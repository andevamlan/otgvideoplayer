package com.androidinspain.otgviewer;

import android.app.Application;

import com.github.mjdev.libaums.fs.UsbFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amlan on 16/09/16.
 */
public class OTGApplication extends Application {

    public static List<UsbFile> VIDEO_FILES;

    @Override
    public void onCreate() {
        super.onCreate();

        VIDEO_FILES = new ArrayList<>();
    }
}
