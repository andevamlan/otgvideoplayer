package com.androidinspain.otgviewer.callbacks;

/**
 * Created by amlan on 16/09/16.
 */
public interface OnScanOver {
    void onScanComplete();
}
