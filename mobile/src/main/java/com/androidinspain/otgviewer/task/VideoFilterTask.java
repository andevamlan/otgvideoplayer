package com.androidinspain.otgviewer.task;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.androidinspain.otgviewer.OTGApplication;
import com.androidinspain.otgviewer.callbacks.OnScanOver;
import com.androidinspain.otgviewer.util.Utils;
import com.github.mjdev.libaums.fs.UsbFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by amlan on 16/09/16.
 */
public class VideoFilterTask extends AsyncTask<UsbFile, Void, Void> {


    private OnScanOver onScanOver;
    private static final String TAG = VideoFilterTask.class.getName();
    private Context context;
    private Handler handler = new Handler();

    public VideoFilterTask(Context context , OnScanOver onScanOver) {
        this.context = context;
        this.onScanOver = onScanOver;
    }

    @Override
    protected Void doInBackground(UsbFile... usbFiles) {

        UsbFile currentDir = usbFiles[0];

        List<UsbFile> files = new ArrayList<>();
        try {
            files = Arrays.asList(currentDir.listFiles());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "files size: " + files.size());

        for (final UsbFile usbFile : files) {
            if (usbFile.isDirectory()) {
                getFilesFromFolder(usbFile);
            } else {
                int index = usbFile.getName().lastIndexOf(".");
                if (index > 0) {
                    String prefix = usbFile.getName().substring(0, index);
                    String ext = usbFile.getName().substring(index + 1);

                    if (Utils.getMimetype(ext.toLowerCase()) != null
                            &&
                            Utils.getMimetype(ext.toLowerCase()).contains("video")) {
                        Log.e(TAG, "mimetype: " + Utils.getMimetype(ext.toLowerCase()) + ". ext is: " + ext);
                        OTGApplication.VIDEO_FILES.add(usbFile);
                        /*handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, usbFile.getName(), Toast.LENGTH_LONG).show();
                            }
                        });*/

                    }
                }
            }
        }
        return null;
    }

    private void getFilesFromFolder(UsbFile folder) {
        List<UsbFile> files = new ArrayList<>();
        try {
            for (final UsbFile usbFile : folder.listFiles()) {
                if (usbFile.isDirectory()) {
                    getFilesFromFolder(usbFile);
                } else {
                    int index = usbFile.getName().lastIndexOf(".");
                    if (index > 0) {
                        String prefix = usbFile.getName().substring(0, index);
                        String ext = usbFile.getName().substring(index + 1);
                        if (Utils.getMimetype(ext.toLowerCase()) != null
                                &&
                                Utils.getMimetype(ext.toLowerCase()).contains("video")) {
                            Log.e(TAG, "mimetype: " + Utils.getMimetype(ext.toLowerCase()) + ". ext is: " + ext);
                            OTGApplication.VIDEO_FILES.add(usbFile);
                            /*handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context, usbFile.getName(), Toast.LENGTH_LONG).show();
                                }
                            });*/
                        }

                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if(onScanOver!=null)
            onScanOver.onScanComplete();
    }

}
